export function setState(state) {
  return {
    type: 'SET_STATE',
    state
  };
}

export function vote(entry) {
  console.log("VOTE");
  return {
    meta: {remote: true},
    type: 'VOTE',
    entry
  };
}

export function next() {
  console.log("NEXT");
  return {
    meta: {remote: true},
    type: 'NEXT'
  };
}
